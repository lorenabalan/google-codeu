import java.util.Lists;
import java.util.ArrayLists;
import java.util.Random;

public class Collection {

	public static void main (String args) {

		List<Integer> input = new ArrayList<Integer>(Arrays.asList(5,3,9,4,3,3,8,3,3)); // test #1
		Integer majority = hasMajority(input); 

		if(majority != null)
			System.out.println(majority);
		else 
			System.out.println("no majority.");

		input = new ArrayList<Integer>(Arrays.asList(5,3,9,4,3,3,8,3)); // test #2
		majority = hasMajority(input);

		if(majority != null)
			System.out.println(majority);
		else
			System.out.println("no majority.");
	}
	public Integer hasMajority(List<Integer> list) {
		
		Integer size = list.size();
		
		if(size == 0)
			return null;

		// order list (Quick Sort)
		quickSort(list, 0, size - 1);
		

		Integer middle = size / 2, maj = size / 2 + 1; // minimum needed to have a majority

		// easy case - if majority is represented by smallest/largest number in the sorted array
		if(list.get(0) == list.get(middle))
			return list.get(0);
		if(list.get(size-1) == list.get(middle))
			return list.get(size-1);

		// If that's not our situation, we start looking into the array from the middle, working our way outwards 
		// and counting to see if we have at least SIZE/2+1 (minimum needed majority) appearances of the same number.
		// The number representing the majority should always be found in the middle of the sorted array.
		Integer left = middle - 1, right = middle + 1, counter = 1;
		boolean keepLeft = false;

		while(list.get(left) == list.get(right)) {
			counter+=2; 
			if(counter >= maj)
				return list.get(left);
			left--;
			right++;
			if(list.get(right) != list.get(middle))
				keepLeft = true;
		}

		// Keep checking for the same number either to the left or to the right only.
		// This is helpful in situations like: _ X X X X X _ _, where the Xs at indexes 3,4,5 
		// have already been counted in the previous while loop ('middle' is index 4), 
		// but we still have (5 - 3) Xs to find in one of the remaining halves. 

		if(keepLeft) { // keep left for (needed_majority - counter) # of steps

			for(int k = left; k > left - (maj - counter); k--)
				if(list.get(k) != list.get(middle))
					return null;

			return list.get(k+1);

		}
		else { // keep right for (needed_majority - counter) # of steps

			for(int k = right; k < right + (maj - counter); k++)
				if(list.get(k) != list.get(middle))
					return null;

			return list.get(k-1);

		}		


		return null;
	}
 
    
	public void quickSort(List<Integer> array, Integer head, Integer tail) {

		Integer pivot; 
		Random rand = new Random(); 
		pivot = rand.nextInt(tail) + head; // choose random number (index) between head and tail -- that will be our pivot

		swap(array, pivot, tail); // swap array[pivot] with array[tail] -- put pivot at the end

		Integer i = head - 1; // helps us keep track of where we should put the pivot at the end of our partitioning stage

		// PARTITIONING stage -- if we find an elements that's smaller or equal to our pivot, we move it to the LHS of the list, keeping larger elements to the RHS of the list
		for (int j = head; j < tail; j++) {
			
			if(array.get(j) <= pivot) {
				i++;
				swap(array, i, j); // swap array[i] & array[j]
			}

		}

		swap(array, tail, i+1); // swap array[tail] & array[i+1] -- put pivot into its rightful place after partition is completed

		// recursive call of sorting algorithm on numbers to the left and to the right of the pivot (IF the respective sub-lists are not singletons)
		if(head < i)
			quickSort(array, head, i);
		if(i+2 < tail)
			quickSort(array, i+2, tail);

	}

	// helper function to reduce code size (swaping elements at index1 and index2 in a given list)
	public void swap(List<Integer> list, Integer index1, Integer index2) {
		
		Integer aux = list.get(index1);
		list.set(index1, list.get(index2));
		list.set(index2, aux);

	}


}
