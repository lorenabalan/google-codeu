import java.util.List;
import java.util.ArrayList;
import java.util.Random;

public class Statistics {

	public List<Integer> numbers;

	// constructor	
	public Statistics(List<Integer> input) {
		this.numbers = new ArrayList<Integer>();
		this.numbers = input;
	}

	// need to use principle of ordered statistics - based on quick sort - to find nth Largest number

	public Integer nthLargest(Integer n) {

		if(numbers.size() == 0)
			return null;

		return randomSelect(numbers, 0, numbers.size()-1, n);

	}

	public Integer randomSelect (List<Integer> array, Integer head, Integer tail, Integer i) {
		if(head == tail) 
			return array.get(head);

		Integer pivot = randomPartition(array, head, tail);
		Integer pivotHere = pivot - head + 1; // index in current array (array[head,...,tail])

		if (pivotHere == i)
			return array.get(pivot);

		if (i < pivotHere)
			return randomSelect(array, head, pivot-1, i);
		else
			return randomSelect(array, pivot+1, tail, i-pivotHere);
	}

	public Integer randomPartition(List<Integer> array, Integer head, Integer tail) {

		Integer rand_pivot; 
		Random rand = new Random(); 
		rand_pivot = rand.nextInt(tail) + head; // choose random number between head and tail

		swap(array, rand_pivot, tail); // swap array[rand_pivot] with array[tail] -- put random pivot at the end

		return partition(array, head, tail);

	}

	public Integer partition(List<Integer> array, Integer head, Integer tail) {

		Integer pivot = array.get(tail); // get pivot, which we know is at the end of the given array
		Integer i = head - 1; // helps us keep track of where we should put the pivot at the end of the partitioning stage

		// PARTITIONING stage -- elements larger than the randomly chosen pivot moved to the left, the rest moved to the right
		for (j = head; j <= tail; j++) {
			
			if(array.get(j) >= pivot) {
				i++;
				swap(array, i, j); // swap array[i] & array[j]
			}

		}

		swap(array, tail, i+1); // swap array[tail] & array[i+1]

		return i+1; // return index of pivot

	}

	// small helper function to reduce code size
	public void swap(List<Integer> list, Integer index1, Integer index2) {
		
		Integer aux;
		aux = list.get(index1);
		list.set(index1, list.get(index2));
		list.set(index2, aux);

	}

}