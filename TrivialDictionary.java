import java.util.Random;

class TrivialDictionary {

	public boolean sizeKnown = false;

	public static String wordAt(Integer index) {
		// implementation
		// assume the dictionary is already alphabetically sorted
	}

	// public constructor - when creating new TrivialDictionary, the boolean field sizeKnown is set to false - could do this straight with a (null) Integer object??

	public static boolean isInDictionary(String word) {
		
		// find size of dictionary -- one-off cost
		if(!sizeKnown) {

			Integer bound = guessUpperBound();
			Integer size = findSize(bound);
			sizeKnown = true;

		}

		// binary search to find word 
		Integer head = 0, tail = size - 1;

		while (head != tail) {
			String mid = wordAt((tail-head)/2);
			int comparison = mid.compareToIgnoreCase(word);

			if(comparison == 0)
				return true;
			else if(comparison < 0)
					tail = (tail-head) / 2;
			else
				head = (tail-head) / 2;
		}

		return false;

		// for extra points - idea :
		// Implement a type of cache when searching for a word? Whenever we search for a new word,
		// we first check the cache (an ArrayList and use contains() method), and if it's not there, 
		// we resort to using wordAt() and add it to the cache (add() method).
		// The cache should have a set size and if it reaches its maximum,we replace the oldest entries
		// with new ones. Have a counter that keeps track of that (pointing at the oldest element in the cache).
		
	}

	public static Integer guessUpperBound() {

		// generate random number;
		// compare to size;
		// if guess >= actual size, return the guessed number
		// if guess < actual size, keep generating random number larger than its predecessor

	}

	public static Integer findSize(Integer upperBound) {

		// binary search to find size of dictionary

		Integer head = 0, tail = upperBound;

		while (head != tail) {
			Integer mid = (tail-head) / 2;

			if(mid == actualSize)
				return true;
			else if(mid < 0)
					tail = (tail-head) / 2;
			else
				head = (tail-head) / 2;
		}

	}
}