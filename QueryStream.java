import java.util.ArrayList;
import java.util.Lists;
import java.util.Iterator;
import java.util.Date;

public class QueryStream implements Iterable<QueryStream> {
	
	public List<Query> queries;
	private int currentSize;

	// constructor
	public QueryStream(List<Query> newArray) {
        this.queries = new ArrayList<Query>(); 
        this.queries = newArray;
        this.currentSize = queries.size();
    }

	
	@Override
    public Iterator<QueryStream> iterator() {
        Iterator<QueryStream> it = new Iterator<QueryStream>() {

            private int currentWordIndex = 0;
            private int currentQueryIndex = 0;

            @Override
            public boolean hasNext() {

            	if(currentQueryIndex < currentSize && queries.get(currentQueryIndex) != null) {
            		String[] queryWords = queries.get(currentQueryIndex).split(" ");

            		if(queryWords[currentWordIndex] != null)
            			return true;
            	}

                return false;
            }

            @Override
            public String next() {

                String[] queryWords = queries.get(currentQueryIndex).split(" ");

                if(queryWords[currentWordIndex] != null)
                	return queryWords[currentWordIndex++];
                else {
                	currentWordIndex = 0;
                	currentQueryIndex++;
                	return "<NEWQUERY>";
                }
            }

        };

        return it;
    }

}

public class Query {
	public Timestamp time;
	public String words;
	// ...plus other fields
}